package br.pb.fabrica.testes;

import org.junit.Test;

import br.pb.fabrica.core.BaseTest;
import br.pb.fabrica.pages.AcessarEstagioPage;
import br.pb.fabrica.pages.CadastrarEstagioPage;

public class CadastrarEstagioTeste extends BaseTest{
	AcessarEstagioPage acessarEstagio = new AcessarEstagioPage();
	CadastrarEstagioPage cadastrar = new CadastrarEstagioPage();
	
	@Test
	public void DeveCadastrarEstagio() {
		acessarEstagio.PagCadastro(); 		
		
		cadastrar.setConcedente("Ortograma");
		cadastrar.setCurso("Medicina");
		cadastrar.setDisciplina("Pediatria");
		cadastrar.setTurno("Manh�");
		cadastrar.setAlunos("98");
		cadastrar.setCustoAluno("3231,01");
		cadastrar.setPreceptor("Wallace");
		cadastrar.setLocal("Maternidade Frei Dami�o");
		cadastrar.setEstabelecimento("Unidade de sa�de");
		cadastrar.setSetor("Gest�o de Pessoas");
		cadastrar.setData("08/10/2020");
		cadastrar.clicarBotaoEnviar();	
	}
	
	@Test
	public void verificarCadastroEstagio() {
		acessarEstagio.ListarCadastro(); 	
		
	}
	
}
