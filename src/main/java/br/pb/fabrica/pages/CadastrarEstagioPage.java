package br.pb.fabrica.pages;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import br.pb.fabrica.core.BasePage;

public class CadastrarEstagioPage extends BasePage {
	
	
	/* Formulario */
	
	  public void setConcedente(String tipo){
        selecionarCombo(By.id("id_tipo_de_convenio"), "Ortotrauma");
    }
	  
	  public void setCurso(String tipo){
	        selecionarCombo(By.id("id_curso"), tipo);
	}  
	
	  public void setDisciplina(String tipo){
	        selecionarCombo(By.id("id_disciplina"), tipo);
	}  
	  
	  public void setTurno(String tipo){
	        selecionarCombo(By.id("id_turno"), tipo);
	}
	  
	  public void setAlunos(String texto){
	        escrever("id_quantidade_de_alunos", texto);
	}  
	  
	  public void setCustoAluno(String texto){
	        escrever("id_custo_por_aluno", texto);
	} 
	  
	  public void setPreceptor(String tipo){
	        selecionarCombo(By.id("id_preceptor"), tipo);
	}
	 
	  public void setLocal(String tipo){
	        selecionarCombo(By.id("id_local"), tipo);
	}
	  
	  public void setEstabelecimento(String tipo){
	        selecionarCombo(By.id("id_tipo_de_estabelecimento"), tipo);
	}
	  
	  public void setSetor(String tipo){
	        selecionarCombo(By.id("id_setor"), tipo);
	}	  
	 
	  public void setData(String texto){
	        escrever("id_dates", texto);
	}
	  
	  public void clicarBotaoEnviar(){
	        clicarBotao("//button[text()='Enviar']");
	    }  

}
