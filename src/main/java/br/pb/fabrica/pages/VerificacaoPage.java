package br.pb.fabrica.pages;

import static org.junit.Assert.assertTrue;
import org.openqa.selenium.WebDriver;

public class VerificacaoPage {
	private WebDriver driver;
	
	public VerificacaoPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public boolean validarQtdAlunos() {
		assertTrue(driver.getPageSource().contains("98"));
		return false;
		}	
	public boolean validarCustoPorAluno() {
		assertTrue(driver.getPageSource().contains("3231,01"));
		return false;
		}	
}

