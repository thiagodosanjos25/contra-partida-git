package br.pb.fabrica.pages;

import org.openqa.selenium.WebDriver;

public class AcessarEstagioPage {
	private WebDriver driver;
	
	public void PagCadastro() 
	{
		driver.get("https://contra-partida.herokuapp.com/gestao/createestagio/");
	}
	
	public void ListarCadastro() 
	{
		driver.get("https://contra-partida.herokuapp.com/gestao/listaestagio/");
	}

}
